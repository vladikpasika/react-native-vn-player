#ifndef VN_KV_H
#define VN_KV_H

#include "vn_os.h"

#ifdef __cplusplus
extern "C"
{
#endif


#pragma pack(push, 1) /* store previous pack value */
                      /* and set new value to 1 (no alignment) */
struct vn_kv_t
{
    void *key;
    void *value;
};

#pragma pack(pop)     /* restore previous pack value */


/** 
 * Create NULL-terminated list of vn_kv_t 
 * 
 * @param size 
 * 
 * @return 
 */
VN_SDK_API vn_kv_t **vn_kv_list_create(unsigned size);

VN_SDK_API void vn_kv_list_destroy(vn_kv_t **kv);


#ifdef __cplusplus
}
#endif




#endif /* VN_KV_H */
