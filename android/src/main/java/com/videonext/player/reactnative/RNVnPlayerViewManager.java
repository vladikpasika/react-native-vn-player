package com.videonext.player.reactnative;

import android.app.Activity;
import javax.annotation.Nullable;

import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.videonext.MediaPlayer;

import java.util.Map;


public class RNVnPlayerViewManager extends SimpleViewManager<MediaPlayer> {
    public static final String REACT_CLASS = "VnPlayer";

    private Activity mActivity = null;

    private ThemedReactContext mContext = null;

    public RNVnPlayerViewManager(Activity activity) {
        super();
        mActivity = activity;
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public VnPlayer createViewInstance(ThemedReactContext context) {
        mContext = context;
        return new VnPlayer(context);
    }

    @ReactProp(name = "source")
    public void setSource(MediaPlayer player, ReadableMap source) {

        player.setSource(source);
    }

    @ReactProp(name = "play")
    public void setPlay(MediaPlayer player, boolean play) {

        if (play)
            player.play();
        else
            player.stop();

    }


    @ReactProp(name = "fullscreen")
    public void setFullscreen(MediaPlayer player, boolean fullscreen) {
        player.setFullscreen(fullscreen);
    }


    @Override
    @Nullable
    public Map getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder builder = MapBuilder.builder();
        builder.put("onStateChanged", MapBuilder.of("registrationName", "onStateChanged"));
        builder.put("onNewFrame", MapBuilder.of("registrationName", "onNewFrame"));
        return builder.build();
    }

}
