package com.videonext;

public class Result<T> {

    public int error = 0;
    public String errorString = null;
    public T value = null;

    public Result(int error, String errorString, T value) {
        this.error = error;
        this.errorString = errorString;
        this.value = value;
    }
}
