/*
#  $Id$
# -----------------------------------------------------------------------------
#  The part of 'VideoNEXT MediaClient SDK'
# -----------------------------------------------------------------------------
#  Author: Petrov Maxim, 03/04/2009
#  Edited by:
#  QA by:
#  Copyright: videoNEXT LLC
# -----------------------------------------------------------------------------
*/

#ifndef MUTEX
#define MUTEX

#ifdef WIN32
#include <winsock2.h>
#include <windows.h>
#else
#include <pthread.h>
#endif

namespace videonext { namespace media {

/**
 * Simple crossplatform mutex implementation
 */
class Mutex
{
public:
    Mutex();
   
    virtual ~Mutex();    

    void lock();
    
    void unlock();    
   
protected:
#ifdef WIN32
    PCRITICAL_SECTION mutex;
#else
    pthread_mutex_t *mutex;
#endif
    
private:
    /**
     * Asignment operator and copy constructur are deprecated. Therefore it is in private section
     */
    Mutex& operator=(const Mutex& m){return *this;}
    Mutex(const Mutex& m){}
    
    void init();
    
    void destroy();    
};


class Cond: public Mutex
{
public:
    Cond();

    virtual ~Cond();
    
    void signal();
    
    void wait();
    
private:
#ifdef WIN32
      HANDLE event;
#else
      pthread_cond_t cond;
#endif    
};


class MutexGuard
{
public:
    MutexGuard(Mutex &mutex);

    virtual ~MutexGuard();

    void acquire();

    void release();

private:
    Mutex &mutex_;
    bool locked_;
};

}}

#endif /* MUTEX */

/*
# ----------===== Changes log =====----------
# $Log$
# Revision 1.2  2006/10/13 11:55:32  mpetrov
# added jump2ts in the buffer playback
#
# Revision 1.1  2006/06/15 09:18:01  mpetrov
# *** empty log message ***
#
# Revision 1.1.1.1  2005/12/06 11:36:28  max
#
#
*/

