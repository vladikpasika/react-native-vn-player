package com.videonext;

import java.util.Map;

public class Entity {
    public int id;
    public Map<String, String> attrs;

    Entity(int id, Map<String, String> attrs) {
        this.id = id;
        this.attrs = attrs;
    }

}
