#import "VnPlayerManager.h"
#import "MediaPlayer.h"
#import "React/RCTBridge.h"

@implementation VnPlayerManager

RCT_EXPORT_MODULE();

@synthesize bridge = _bridge;

- (UIView *)view
{
  return [[VnPlayer alloc] initWithEventDispatcher:self.bridge.eventDispatcher];
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

/*
 uri: URI
 width: initial width of player
 height: initial height of player
 */
RCT_EXPORT_VIEW_PROPERTY(source, NSDictionary);
RCT_EXPORT_VIEW_PROPERTY(play, BOOL);
RCT_EXPORT_VIEW_PROPERTY(fullscreen, BOOL);

RCT_EXPORT_VIEW_PROPERTY(onStateChanged, RCTBubblingEventBlock);
RCT_EXPORT_VIEW_PROPERTY(onNewStream, RCTBubblingEventBlock);
RCT_EXPORT_VIEW_PROPERTY(onNewFrame, RCTBubblingEventBlock);


@end
