
#import <UIKit/UIKit.h>
#include "vn_player.h"
#include "Mutex.h"
#import "React/RCTView.h"

using namespace videonext;
using namespace videonext::media;

// forward declaration
namespace videonext { namespace media { namespace iphone {
	class MediaPlayer;
}}}

@class RCTEventDispatcher;

@interface VnPlayer : UIView
{
	CGColorSpaceRef colorspaceRef;  /// cached colorspace
	CGDataProviderRef dataProviderRef; /// cached dataprovider
	
	videonext::media::iphone::MediaPlayer *player;
	
	BOOL updateImages;
	BOOL stopped;
    BOOL fullscreen;
    std::string uri;
    
    int outputWidth;
    int outputHeight;
}
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)initWithEventDispatcher:(RCTEventDispatcher *)eventDispatcher;// NS_DESIGNATED_INITIALIZER;

// callbacks invoked by MediaPlayer
- (void) displayFrame:(id)cbParams;
- (void) stateChanged:(id)cbParams;
- (void) newStream:(id)cbParams;

@property (nonatomic, retain) RCTEventDispatcher *eventDispatcher;
@property (strong) UIImageView* imageView; 

// RCT events
@property (nonatomic, copy) RCTBubblingEventBlock onStateChanged;
@property (nonatomic, copy) RCTBubblingEventBlock onNewStream;
@property (nonatomic, copy) RCTBubblingEventBlock onNewFrame;

@end

