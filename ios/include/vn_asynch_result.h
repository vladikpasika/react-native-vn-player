#ifndef _VN_ASYNCH_RESULT_H_
#define _VN_ASYNCH_RESULT_H_

#include "vn_os.h"
#include "vn_result.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct vn_asynch_result_t;

typedef void (*vn_asynch_result_callback_t) (vn_asynch_result_t *, void *user_data);

VN_SDK_API vn_asynch_result_t *vn_asynch_result_create();

VN_SDK_API void vn_asynch_result_destroy(vn_asynch_result_t *);

VN_SDK_API int vn_asynch_result_is_ready(vn_asynch_result_t *);

/** 
 * The callback will be invoked when the result gets set. If the result is already set when this function gets invoked, then the callbackwill be invoked immediately.
 */
VN_SDK_API void vn_asynch_result_set_callback(vn_asynch_result_t *vn_asynch_result, vn_asynch_result_callback_t callback, void *user_data);

VN_SDK_API vn_result_t *vn_asynch_result_get(vn_asynch_result_t *ar, int timeout_ms);

#ifdef __cplusplus
}
#endif

#endif /* _VN_ASYNCH_RESULT_H_ */


