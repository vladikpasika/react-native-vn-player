package com.videonext;

public class AsynchResult<T> {
    private Result<T> result = null;
    private AsynchResultObserver<T> observer = null;

    public synchronized void set(Result<T> result) {
        this.result = result;
        if (observer != null)
            observer.update(result);

        this.notify();
    }

    public synchronized Result<T> get(long millis) {
        if (result == null) {
            try {
                this.wait(millis);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return result;
    }

    public synchronized void setObserver(AsynchResultObserver<T> o) {
        observer = o;
        if (result != null)
            o.update(result);
    }

}
