#ifndef _VN_RENDERER_H_
#define _VN_RENDERER_H_

#include "vn_player.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct vn_renderer_context_t; /* opaque */

enum VN_IMAGE_FILL_MODE {
    VN_IMAGE_FILL_ASPECT, // follow aspect ration
    VN_IMAGE_FILL_STRETCH // stretch frame to drawing area 
};

enum VN_IMAGE_ZOOM_MODE {
    VN_IMAGE_ZOOM_BILENEAR, 
    VN_IMAGE_ZOOM_NONE 
};

struct vn_image_calibration_t
{
    /**
     * The brightness offset (-1..1). Default: 0
     */
    float brightness;

    /**
     * The contrast multiplier (0..2). Default: 1.0
     */
    float contrast;

    /**
     * The saturation offset (1..5). Default 1.0
     */
    float saturation;
};
    
struct vn_image_pan_zoom_t
{
    /**
     * X coord of center of pan (0.0-1.0). Default 0.5 (X center)
     */
    float pan_x;

    /**
     * Y coord of center of pan (0.0-1.0). Default 0.5 (Y center)
     */
    float pan_y;

    /**
     * scale level (1-5). Default 1 (no scale)
     */    
    float scale;

    VN_IMAGE_ZOOM_MODE mode;
};
    
/** 
 * Create (if possible for such platform) renderer
 * 
 * 
 * @return Non-NULL pointer if sucess
 */
VN_SDK_API vn_renderer_context_t *vn_renderer_create(void *window);

    
VN_SDK_API VN_PLAYER_PIXEL_FORMAT vn_renderer_get_preferred_pixel_format(vn_renderer_context_t *ctx);

/** 
 * Process frame for rendering
 * 
 * @param ctx 
 * @param frame  
 * 
 * @return true if success
 */
VN_SDK_API int vn_renderer_process(vn_renderer_context_t *ctx,
                                   const vn_player_frame_t *frame);

/** 
 * Paint previously proccessed frame
 * 
 * @param ctx 
 * @param mode Renderer mode
 * @param ic Image Calibartion, can be NULL
 * @param pz Pan&Zoom, can be NULL
 * 
 * @return true if success
 */
VN_SDK_API int vn_renderer_paint(vn_renderer_context_t *ctx, VN_IMAGE_FILL_MODE mode, struct vn_image_calibration_t *ic, struct vn_image_pan_zoom_t *pz);

VN_SDK_API void vn_renderer_destroy(vn_renderer_context_t *ctx);
    
#ifdef __cplusplus
}
#endif

#endif /* _VN_RENDERER_H_ */
