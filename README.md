
# react-native-vn-player

## Getting started

`$ npm install react-native-vn-player --save`

### Mostly automatic installation

`$ react-native link react-native-vn-player`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-vn-player` and add `RNVnPlayer.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNVnPlayer.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.videonext.player.reactnative.RNVnPlayerPackage;` to the imports at the top of the file
  - Add `new RNVnPlayerPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-vn-player'
  	project(':react-native-vn-player').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-vn-player/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-vn-player')
  	```


## Usage
```javascript
import RNVnPlayer from 'react-native-vn-player';

// TODO: What to do with the module?
RNVnPlayer;
```
  