#ifndef VN_RESULT_H
#define VN_RESULT_H

#include "vn_os.h"
#include "vn_kv_list.h"

#define E_VN_INTERNAL_ERROR        10100

#ifdef __cplusplus
extern "C"
{
#endif

#pragma pack(push, 1) /* store previous pack value */
                      /* and set new value to 1 (no alignment) */
struct vn_result_t
{
    int error_code;
    char *error_string;
    vn_kv_t **kvl;
};

#pragma pack(pop)     /* restore previous pack value */

VN_SDK_API vn_result_t *vn_result_create(int error_code, const char *error_string);

VN_SDK_API void vn_result_destroy(vn_result_t *r);

VN_SDK_API void vn_result_set_error(vn_result_t *r, int error_code, const char *error_fmt, ...);

#ifdef __cplusplus
}
#endif

#endif /*VN_RESULT_H*/
