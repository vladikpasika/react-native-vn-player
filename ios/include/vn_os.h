#ifndef VN_OS_H
#define VN_OS_H

#ifdef WIN32
   #include <winsock2.h> // struct timeval

   #ifdef VN_SDK
      #define VN_SDK_API __declspec(dllexport)
   #else
      #ifndef VN_SDK_STATIC
         #define VN_SDK_API __declspec(dllimport)
      #else /*!VN_SDK_STATIC*/
         #define VN_SDK_API
      #endif /*VN_SDK_STATIC*/
   #endif
#else /*!WIN32*/
   #define VN_SDK_API
   #include <sys/time.h>
#endif

#endif /*VN_OS_H*/
