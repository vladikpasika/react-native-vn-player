#ifndef _VN_MALLOC_H_
#define _VN_MALLOC_H_

#include "vn_os.h"
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef void (*vn_custom_free_callback_t) (void *data);

VN_SDK_API void *vn_malloc(size_t size, vn_custom_free_callback_t free_cb);

VN_SDK_API void vn_free(void *ptr);


#ifdef __cplusplus
}
#endif


#endif /* _VN_MALLOC_H_ */
