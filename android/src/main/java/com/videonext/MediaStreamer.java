package com.videonext;

import android.util.Log;
import java.util.Map;

public class MediaStreamer {
    static {
        System.loadLibrary("VNMediaClient-jni");
    }

    private long id = 0;

    public MediaStreamer() {}

    // VN_JStreamer
    public void initLiveStreamer(String url, int width, int height) throws Exception {
        try {
            id = nativeLiveStreamerInit(url, width, height);
        } catch (Exception e) {
            Log.e("--", e.getMessage());
        }

        if (id == 0)
            throw new Exception("LiveStreamer initialization failed");
    }

    public void destroyLiveStreamer() {
        if (id != 0) {
            nativeLiveStreamerDestroy(id);
            id = 0;
        }
    }

    public void sendFrameThroughLiveStreamer(byte[] img, float gpsLat, float gpsLong, float gpsAlt) throws Exception {
        if (id == 0)
            throw new Exception("LiveStreamer not ready");

        int ret = nativeLiveStreamerSendVideo(id, img, gpsLat, gpsLong, gpsAlt);
        if (ret != 0)
            throw new Exception("Failed to send video frame through LiveStreamer. Error: " + ret);
    }


    // VN_JFileStreamer
    public void initFileStreamer(String url, String filepath, long creation_ts, Map<String, String> info, boolean downscale, long last_pos) throws Exception {
        try {
            id = nativeFileStreamerInit(url, filepath, creation_ts, info, downscale, last_pos);
        } catch (Exception e) {
            Log.e("--", e.getMessage());
        }

        if (id == 0)
            throw new Exception("FileStreamer initialization failed");
    }

    public void destroyFileStreamer() {
        if (id != 0) {
            nativeFileStreamerDestroy(id);
            id = 0;
        }
    }

    public int postInitFileStreamer(String url, boolean downscale) throws Exception {
        if (id == 0)
            throw new Exception("FileStreamer is not ready");

        return nativeFileStreamerPostInit(id, url, downscale);
    }

    public int sendFrameThroughFileStreamer(Long last_pos, Long progress) throws Exception {
        if (id == 0)
            throw new Exception("FileStreamer is not ready");

        return nativeFileStreamerSendVideo(id, last_pos, progress);
        /*
        if (ret != 0)
            throw new Exception("Failed to send video frame through FileStreamer. Error: " + ret);*/
    }

    // VN_JStreamer
    private native long nativeLiveStreamerInit(String url, int width, int height);
    private native void nativeLiveStreamerDestroy(long id);
    private native int nativeLiveStreamerSendVideo(long id, byte[] img, float gpsLat, float gpsLong, float gpsAlt);

    // VN_JFileStreamer
    private native long nativeFileStreamerInit(String url, String fpath, long creation_ts, Map<String, String> info, boolean downscale, long last_pos);
    private native void nativeFileStreamerDestroy(long id);
    private native int nativeFileStreamerPostInit(long id, String url, boolean downscale);
    private native int nativeFileStreamerSendVideo(long id, Long last_pts, Long progress);
}
