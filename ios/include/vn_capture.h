#ifndef _VN_CAPTURE_H_
#define _VN_CAPTURE_H_

#include "vn_os.h"

#ifdef __cplusplus
extern "C"
{
#endif

struct vn_capture_context_t; /* opaque */

struct vn_capture_callbacks_t
{
    void (*on_error)(int error, const char *error_message, void *client_data);
    void (*on_audio_stats)(short samples[], unsigned samples_count, void *client_data);
    void (*on_new_preview_image)(void *client_data);
    void* (*create_image_preview_buffer)(int width, int height, void *client_data);
    void (*lock_image_preview_buffer)(void *client_data);
    void (*unlock_image_preview_buffer)(void *client_data);
};

VN_SDK_API vn_capture_context_t *vn_capture_create();

VN_SDK_API void vn_capture_set_callbacks(vn_capture_context_t *ctx, const vn_capture_callbacks_t *callbacks, void *client_data);

/** 
 * Get all known audio capture devices
 * 
 * @return NULL-terminated array of strings
 */
VN_SDK_API char **vn_capture_get_audio_devices(vn_capture_context_t *);

/** 
 * Get all known video capture devices
 * 
 * @return NULL-terminated array of strings
 */
VN_SDK_API char **vn_capture_get_video_devices(vn_capture_context_t *);


/** 
 * Get all known video device parameters
 * 
 * @return NULL-terminated array of strings in format "WxH/PixelFormat/FPS", ex: "{1920x1080/I420/30, 1280x720/I420/30, NULL}"
 */
VN_SDK_API char **vn_capture_get_video_device_parameters(vn_capture_context_t *, const char *device_name);

/** 
 * Start a/v capturing
 * 
 * @param audio_capture_device 
 * @param video_capture_device 
 * @param video_params String in format "WxH/PixelFormat/FPS", ex: "1280x720/I420/30"
 * @param rtsp_url 
 * @param bit_rate Video bitrate 
 * 
 * @return 
 */
VN_SDK_API int vn_capture_start(vn_capture_context_t *ctx, const char *audio_capture_device, const char *video_capture_device,
                               const char *video_params, const char *rtsp_url, int bit_rate);

/** 
 * Stop a/v capturing. Capturing can be started again using vn_capture_start(...)
 * 
 */
VN_SDK_API void vn_capture_stop(vn_capture_context_t *ctx);


VN_SDK_API void vn_capture_destroy(vn_capture_context_t *ctx);

    
#ifdef __cplusplus
}
#endif
#endif /* _VN_CAPTURE_H_ */
