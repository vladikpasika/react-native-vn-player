package com.videonext;

public interface AsynchResultObserver<T> {
    void update(Result<T> result);
}
